const Koa = require('koa')
const app = new Koa()
const views = require('koa-views')
const index = require('./routes/batchUpload')
const continueUpload = require('./routes/continueUpload')
const page = require('./routes/render')

app.use(require('koa-static')(__dirname + '/views'))

app.use(
  views('views', {
    root: __dirname + '/views',
  }),
)

app.use(index.routes(), index.allowedMethods())
app.use(page.routes(), index.allowedMethods())
app.use(continueUpload.routes(), index.allowedMethods())

app.listen(3011, () => {
  console.log(3011)
})

module.exports = app
