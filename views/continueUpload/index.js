const target = document.querySelector('input')

const SIZE = 1024 * 1024 * 0.4 // 每片大小
// const SIZE = 1024 * 1024 * 10; // 每片大小10M

target.onchange = (e) => {
  // 所有的上传文件
  console.log(e.target.files)
  const [file] = e.target.files
  upload(file)
}

function upload(file) {
  // 文件大小
  const size = file.size
  const filename = file.name
  // 切片数量
  const count = Math.ceil(size / SIZE)

  // 第一片试探上传，用于查看是否有已存在数据
  findFile({ filename })
    .then(({ count }) => {
      return count
    })
    .then((uploadCount) => {
      if (uploadCount == count) {
        console.log('此资源文件已上传完毕，不用再次上传')
        return
      }

      fileUpload({ start: uploadCount, count, SIZE, size, filename, file })
    })
}

// start 为开始上传的文件切片编号
function fileUpload({ start, count, SIZE, size, filename, file }) {
  // 切片编号
  let num = start

  // 开启上传
  while (num < count) {
    const min = Math.min(SIZE, size - num * SIZE)

    const [start, end] = [num * SIZE, num * SIZE + min]

    // 本次需要上传的内容
    const uploadContent = file.slice(start, end)

    uploadData({
      data: uploadContent,
      filename,
      count,
      num,
    }).then((data) => {
      console.log(data)
    })

    num++
  }
}

function uploadData({ data, filename, count, num, key }) {
  return new Promise((resolve) => {
    // 二进制传输
    const formD = new FormData()
    formD.append('filename', filename)
    formD.append('chunk', data)
    formD.append('key', key)
    // 分片编号
    formD.append('chunkname', num)
    formD.append('totalCount', count)

    const xhr = new XMLHttpRequest()

    xhr.open('post', `/continue/upload?filename=${encodeURI(filename)}`)

    xhr.onload = function () {
      if (this.status == 200) {
        console.log(this.response)
      }
    }

    xhr.send(formD)
  })
}

function findFile({ filename }) {
  return new Promise((resolve) => {
    const xhr = new XMLHttpRequest()
    xhr.open('get', `/continue/count?filename=${encodeURI(filename)}`)
    xhr.onload = function () {
      if (this.status == 200) {
        resolve(JSON.parse(this.response))
      }
    }
    xhr.send()
  })
}
