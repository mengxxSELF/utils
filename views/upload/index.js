const target = document.querySelector("input");

const SIZE = 1024 * 1024 * 0.4; // 每片大小
// const SIZE = 1024 * 1024 * 10; // 每片大小10M

target.onchange = (e) => {
  // 所有的上传文件
  console.log(e.target.files);
  const [file] = e.target.files;
  upload(file);
};

function upload(file) {
  // 切片编号
  let num = 0;
  // 文件大小
  const size = file.size;
  const filename = file.name;
  // 切片数量
  const count = Math.ceil(size / SIZE);
  // 唯一key 每一个文件的key都不同
  const key = Date.now();

  // 开启上传
  while (num < count) {
    const min = Math.min(SIZE, size - num * SIZE);

    const [start, end] = [num * SIZE, num * SIZE + min];

    // 本次需要上传的内容
    const uploadContent = file.slice(start, end);

    uploadData({
      data: uploadContent,
      filename,
      count,
      num,
      key,
      type: filename.split(".").pop(),
    });

    num++;
  }
}

function uploadData({ data, filename, count, num, key, type }) {
  // 二进制传输
  const formD = new FormData();
  formD.append("filename", filename);
  formD.append("chunk", data);
  formD.append("key", key);
  // 分片编号
  formD.append("chunkname", num);
  formD.append("totalCount", count);

  const xhr = new XMLHttpRequest();

  xhr.open("post", `/batch/upload?fileKey=${key}&type=${type}`);

  xhr.onload = function () {};

  xhr.send(formD);
}
