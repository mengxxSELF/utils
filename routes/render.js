const router = require('koa-router')()

router.get('/upload', async (ctx, next) => {
  await ctx.render('upload/index')
})

router.get('/continueUpload', async (ctx, next) => {
  await ctx.render('continueUpload/index')
})

module.exports = router
