const multiparty = require('multiparty')
const router = require('koa-router')()
const fs = require('fs')
const path = require('path')

const dir = path.resolve(__dirname, '../temp')

router.prefix('/batch')

function createDir(dir) {
  if (fs.existsSync(dir)) return
  fs.mkdirSync(dir)
}

// 记录文件总数值
const tempFiles = new Map()

function saveTemp(form, req, fileKey) {
  return new Promise((resolve, reject) => {
    form.parse(req, (err, fields, files) => {
      // 接收到文件参数时，触发file事件
      // console.log(fields, files);

      const [{ path }] = files.chunk

      const { chunkname, key, totalCount } = fields

      tempFiles.set(fileKey, {
        totalCount: totalCount[0],
        current: chunkname[0],
      })

      // 修改名称
      fs.rename(path, dir + '/tmp_' + key + '_' + chunkname[0], (err) => {
        console.log('err', err)
        reject(err)
      })
    })

    form.on('close', function (args) {
      resolve(fileKey)
    })
  })
}

async function sleep() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(200)
    }, 5000)
  })
}

// 合并文件
async function mergeFile({ fileKey, type }) {
  const files = await fs.readdirSync(dir)

  // 筛出目标文件
  const keyFiles = files.filter((item) => item.split('_').includes(fileKey))

  keyFiles.sort((a, b) => {
    const [, , aFile] = a.split('_')
    const [, , bFile] = b.split('_')
    return aFile - bFile
  })

  createDir('source')

  const writeStream = fs.createWriteStream('./source/' + fileKey + '.' + type)
  for (const file of keyFiles) {
    const path = dir + '/' + file

    const isExist = await fs.existsSync(path)

    if (isExist) {
      writeStream.write(fs.readFileSync(path))
    }
  }

  writeStream.end()
}

router.post('/upload', async (ctx, next) => {
  const { fileKey, type } = ctx.query

  createDir(dir)

  const form = new multiparty.Form({
    uploadDir: dir,
  })

  form.on('error', function (err) {
    console.log('Error parsing form: ' + err.stack)
  })

  await saveTemp(form, ctx.req, fileKey)

  const { totalCount, current } = tempFiles.get(fileKey)

  if (Number(current) + 1 == totalCount) {
    await sleep()

    mergeFile({ fileKey, type })

    ctx.body = { code: 201, msg: ' finish ' }
  } else {
    ctx.body = { code: 200, msg: ' receiving ' }
  }
})

module.exports = router
