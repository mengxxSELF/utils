
module.exports = {
  endOfLine: 'lf',
  tabWidth: 2,
  singleQuote: true,
  semi: false,
  trailingComma: 'all',
  flow: true,
  printWidth: 80,
};
